import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

export const highlightAnimation = trigger('highlightPosition', [
  state('first',  style({transform: 'translateX(0)'})),
  state('second', style({transform: 'translateX(0)'})),
  state('third',  style({transform: 'translateX(0)'})),
  transition('void => first', [
    style({transform: 'translateX(-100%)'}),
    animate(150)
  ]),
  transition('first => second', [
    style({transform: 'translateX(-100%)'}),
    animate(150)
  ]),
  transition('second => third', [
    style({transform: 'translateX(-100%)'}),
    animate(150)
  ]),
  transition('third => void', [
    animate(150, style({transform: 'translateX(100%)'}))
  ])
]);
