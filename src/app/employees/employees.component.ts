import { Component, OnInit } from '@angular/core';

import { Employee } from './employee.class';
import { EMPLOYEES } from './employees.mock';
import { highlightAnimation } from './employees.animations';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: [
    './employees.component.scss',
    './employees.highlight.scss'
  ],
  animations: [highlightAnimation]
})
export class EmployeesComponent implements OnInit {
  employees = EMPLOYEES;
  selectedEmployees: Employee[] = [];

  constructor() { }
 
  ngOnInit() { }

  getPosition(employee: Employee): string {
    const orderMapping = ['first', 'second', 'third'];
    const index = this.selectedEmployees.indexOf(employee);

    return orderMapping[index];
  }

  onSelect(employee: Employee): void {
    if (employee == this.selectedEmployees[0]) {
      return;
    }

    this.selectedEmployees = [
      employee,
      ...this.selectedEmployees.slice(0,2)
    ];
  }
}
