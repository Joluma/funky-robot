# FunkyTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.2.

## Test it live

You can go to the [Live demo](https://joluma.gitlab.io/funky-robot/) to test the app.

## Test it locally

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
